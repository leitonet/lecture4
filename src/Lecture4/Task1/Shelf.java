package Lecture4.Task1;

public class Shelf {
    private Book [] books;

    {
        books = generate();
    }

    private Book [] generate(){
        Book[] books = new Book[4];
        books[0] = new Book("Война и мир", "Толстой", 1873);
        books[1] = new Book("Анна Каренина", "Толстой", 1878);
        books[2] = new Book("Идиот", "Достоевский", 1868);
        books[3] = new Book("Искусство дзюдо", "Путин", "ОлмаМедиаГрупп", "Ротенберг, Черных, Левицкий", 2016);

        return books;
    }

    //Нахожу самую старую книгу и вывожу её автора
    public void findOldestBook() {
        int minYear = books[0].year;
        for (Book book : books) {
            if (book.year < minYear) {
                minYear = book.year;
            }
        }
        for (Book book : books) {
            if (minYear == book.year) {
                System.out.println("Автор самой старой книги: " + book.author);
            }
        }
    }

    //Нахожу книгу определенного автора
    public void findBookByAuthor(String author) {
        for (Book book : books) {
            if (book.author.equals(author)) {
                System.out.println("Книги автора " + author + " : " + book.title);
            }
        }
    }

    //Нахожу книги изданные ранее указанного года
    public void findBooksOlderPresetYear(int year) {
        System.out.println("Книги, изданные ранее " + year + " года:");
        for (Book book : books) {
            if (book.year < year) {
                System.out.println(" - Книга - \"" + book.title + "\", автор - \"" + book.author + "\", год издания - " + book.year);

            }
        }
    }

    //1.1
    //Нахожу книги определенного автора, написанные в соавторстве с кем-то
    public void findBookWithCoauthor() {
        for (Book book : books) {
            if (book.author.equals("Путин") && book.coauthor != null) {
                System.out.println("Книги автора Путин: " + book.title + ". Издательство - \"" + book.publishes + "\"." +
                        " Соавторы: " + book.coauthor + ". Год издания - " + book.year);
            }
        }
    }

    //Нахожу книги, где 3 и более авторов
    public void findBooksWhereMoreThanTreeAuthor() {
        String[] coauthorArray;
        for (Book book : books) {
            if (book.coauthor != null) {
                coauthorArray = book.coauthor.split(", ");
                if(coauthorArray.length >= 3){
                    System.out.println("Книга с 3 и больше соавторами: " + book.title + ". Автор: " + book.author + ". Издательство - \"" + book.publishes + "\"." +
                            " Соавторы: " + book.coauthor + ". Год издания - " + book.year);
                }
            }

        }
    }

}
