package Lecture4.Task1;

public class Book {
    public String title;
    public String author;
    public String publishes;
    public String coauthor;
    public int year;

    public Book(String title, String author, int year){
        this.title = title;
        this.author = author;
        this.year = year;
    }

    public Book(String title, String author, String publishes, String coauthor, int year){
        this.title = title;
        this.author = author;
        this.year = year;
        this.publishes = publishes;
        this.coauthor = coauthor;
    }
}
